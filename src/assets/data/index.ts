import images from '../images';

/* eslint-disable import/prefer-default-export */
export const solution1 = [
    {
        title: 'Vận hành đơn giản',
        content: [
            'Tăng mức độ phủ sóng theo khu vực / Địa lý / ISP',
            'Cải thiện hiệu suất tổng thể',
            'Dự phòng / Chuyển đổi dự phòng',
            'Giúp hệ thống hoạt động tốt, đáp ứng lượng CCU lớn.',
        ],
        img: images.content1_1,
    },
    {
        title: 'Nâng cao chất lượng dịch vụ',
        content: [
            'Tăng mức độ phủ sóng theo khu vực / Địa lý / ISP',
            'Cải thiện hiệu suất tổng thể',
            'Dự phòng / Chuyển đổi dự phòng',
            'Giúp hệ thống hoạt động tốt, đáp ứng lượng CCU lớn.',
        ],
        img: images.content1_2,
    },
    {
        title: 'Tăng trải nghiệm người',
        content: [
            'Tăng mức độ phủ sóng theo khu vực / Địa lý / ISP',
            'Cải thiện hiệu suất tổng thể',
            'Dự phòng / Chuyển đổi dự phòng',
            'Giúp hệ thống hoạt động tốt, đáp ứng lượng CCU lớn.',
        ],
        img: images.content1_3,
    },
    {
        title: 'Tiết giảm chi phí',
        content: [
            'Tăng mức độ phủ sóng theo khu vực / Địa lý / ISP',
            'Cải thiện hiệu suất tổng thể',
            'Dự phòng / Chuyển đổi dự phòng',
            'Giúp hệ thống hoạt động tốt, đáp ứng lượng CCU lớn.',
        ],
        img: images.content1_4,
    },
    {
        title: 'Cung cấp thông tin đầy đủ',
        content: [
            'Tăng mức độ phủ sóng theo khu vực / Địa lý / ISP',
            'Cải thiện hiệu suất tổng thể',
            'Dự phòng / Chuyển đổi dự phòng',
            'Giúp hệ thống hoạt động tốt, đáp ứng lượng CCU lớn.',
        ],
        img: images.content1_5,
    },
];

export const solution2 = [
    {
        url: images.content2_1,
        title: 'DRM',
        text: 'Sigma DRM kiểm soát và bảo vệ dữ liệu video, audio qua mạng Internet',
    },
    {
        url: images.content2_2,
        title: 'Less CDN',
        text: 'Giải pháp truyền tải nội dung media tới người dùng, giúp tiết kiệm chi phí.',
    },
    {
        url: images.content2_3,
        title: 'Multi Screen',
        text: 'Quản lý số lượng thiết bị truy cập video theo người dùng trong thời gian thực.',
    },
];

export const solution3 = [
    {
        url: images.content3_1,
        dateTime: '29/03/2022',
        text: 'Hidden universe revealed in stunning first images from German telescope ',
    },
    {
        url: images.content3_2,
        dateTime: '29/03/2022',
        text: 'Hidden universe revealed in stunning first images from German telescope ',
    },
    {
        url: images.content3_3,
        dateTime: '29/03/2022',
        text: 'Hidden universe revealed in stunning first images from German telescope ',
    },
];

export const news = [
    {
        url: images.content3_1,
        dateTime: '29/03/2022',
        text: 'Hidden universe revealed in stunning first images from German telescope ',
    },
    {
        url: images.content3_2,
        dateTime: '29/03/2022',
        text: 'Hidden universe revealed in stunning first images from German telescope ',
    },
    {
        url: images.content3_3,
        dateTime: '29/03/2022',
        text: 'Hidden universe revealed in stunning first images from German telescope ',
    },
    {
        url: images.content3_3,
        dateTime: '29/03/2022',
        text: 'Hidden universe revealed in stunning first images from German telescope ',
    },
    {
        url: images.content3_1,
        dateTime: '29/03/2022',
        text: 'Hidden universe revealed in stunning first images from German telescope ',
    },
    {
        url: images.content3_2,
        dateTime: '29/03/2022',
        text: 'Hidden universe revealed in stunning first images from German telescope ',
    },
    {
        url: images.content3_2,
        dateTime: '29/03/2022',
        text: 'Hidden universe revealed in stunning first images from German telescope ',
    },
    {
        url: images.content3_3,
        dateTime: '29/03/2022',
        text: 'Hidden universe revealed in stunning first images from German telescope ',
    },
    {
        url: images.content3_1,
        dateTime: '29/03/2022',
        text: 'Hidden universe revealed in stunning first images from German telescope ',
    },
];

export const partner = [
    images.content1_1,
    images.content1_2,
    images.content1_3,
    images.content1_2,
    images.content1_1,
    images.content1_3,
];
