/* eslint-disable global-require */
const images = {
    lightLogo: require('./light_logo.svg').default,
    darkLogo: require('./dark_logo.svg').default,
    banner: require('./banner.png'),
    banner2: require('./Banner2.png'),
    banner3: require('./Banner3.png'),
    vnLanguage: require('./vietnam.png'),
    ukLanguage: require('./united-kingdom.png'),
    franceLanguage: require('./france.png'),
    content1_1: require('./content1_1.png'),
    content1_2: require('./content1_2.png'),
    content1_3: require('./content1_3.png'),
    content1_4: require('./content1_4.png'),
    content1_5: require('./content1_5.png'),
    content2_1: require('./content2_1.png'),
    content2_2: require('./content2_2.png'),
    content2_3: require('./content2_3.png'),
    content3_1: require('./content3_1.png'),
    content3_2: require('./content3_2.png'),
    content3_3: require('./content3_3.png'),
};

export default images;
