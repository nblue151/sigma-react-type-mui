import React, { useLayoutEffect } from 'react';
import { BrowserRouter, Routes, Route, useLocation } from 'react-router-dom';
import './App.css';
import DefaultLayout from './layouts/DefaultLayout';
import Contact from './pages/Contact';
import News from './pages/News';
import Partner from './pages/Partner';
import Solution from './pages/Solution';

interface PropsScroll {
    children: React.ReactElement;
}

// React Router Dom v6: Scroll To Top on Route Change
const ScrollToTop = ({ children }: PropsScroll) => {
    const location = useLocation();
    useLayoutEffect(() => {
        document.documentElement.scrollTo(0, 0);
    }, [location.pathname]);
    return children;
};

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <ScrollToTop>
                    <Routes>
                        <Route
                            path="/"
                            element={
                                <DefaultLayout>
                                    <Solution />
                                </DefaultLayout>
                            }
                        />
                        <Route
                            path="/news"
                            element={
                                <DefaultLayout>
                                    <News />
                                </DefaultLayout>
                            }
                        />
                        <Route
                            path="/partner"
                            element={
                                <DefaultLayout>
                                    <Partner />
                                </DefaultLayout>
                            }
                        />
                        <Route
                            path="/contact"
                            element={
                                <DefaultLayout>
                                    <Contact />
                                </DefaultLayout>
                            }
                        />
                    </Routes>
                </ScrollToTop>
            </div>
        </BrowserRouter>
    );
}

export default App;
