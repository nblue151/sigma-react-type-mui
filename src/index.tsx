import React from 'react';
import ReactDOM from 'react-dom/client';
import { createTheme, CssBaseline, ThemeProvider } from '@mui/material';

import './index.css';
import { Provider } from 'react-redux';
import App from './App';
import { customTheme, lightTheme, darkTheme } from './theme';
import store from './state/store';
import { useAppSelector } from './state/hooks';

interface ThemeProps {
    children: React.ReactNode;
}

export default function Theme({ children }: ThemeProps) {
    const mode = useAppSelector((state) => state.themeGlobal.mode);
    const theme = React.useMemo(
        () =>
            createTheme({
                ...(mode === 'light' ? { ...lightTheme, ...customTheme } : { ...darkTheme, ...customTheme }),
            }),
        [mode]
    );

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            {children}
        </ThemeProvider>
    );
}

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <Theme>
                <App />
            </Theme>
        </Provider>
    </React.StrictMode>
);

declare module '@mui/material/styles' {
    interface Palette {
        shades: Palette['primary'];
        darkNav: Palette['primary'];
        textNav: Palette['primary'];
    }
    interface PaletteOptions {
        shades: PaletteOptions['primary'];
        darkNav: PaletteOptions['primary'];
        textNav: PaletteOptions['primary'];
    }
    interface PaletteColor {
        darker?: string;
    }
    interface SimplePaletteColorOptions {
        darker?: string;
    }
}

declare module '@mui/material/Button' {
    interface ButtonPropsColorOverrides {
        shades: true;
    }
}
