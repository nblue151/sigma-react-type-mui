import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';

const BoxBotomText = styled(Box)(({ theme }) => ({
    width: '24px',
    height: '3px',
    borderRadius: '2px',
    backgroundColor: theme.palette.primary.main,
}));
export default BoxBotomText;
