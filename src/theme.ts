import { ThemeOptions } from '@mui/material';

export const customTheme: ThemeOptions = {};

export const lightTheme: ThemeOptions = {
    palette: {
        primary: {
            main: '#5a27ed',
            light: '#A6AEF7',
        },
        error: {
            main: '#D83131',
        },
        shades: {
            main: '#6f788b',
        },
        background: {
            default: '#fff',
            paper: '#eee',
        },
        text: {
            primary: '#11152A',
            secondary: '#1B1F3B',
        },
        textNav: {
            main: '#fff',
            light: '#dadada',
        },

        darkNav: {
            main: '#282933',
            light: '#333',
        },
    },
};

export const darkTheme: ThemeOptions = {
    palette: {
        mode: 'dark',
        primary: {
            main: '#5a27ed',
            light: '#491CC9',
        },
        error: {
            main: '#D83131',
        },
        shades: {
            main: '#6f788b',
        },
        text: {
            primary: '#fff',
            secondary: '#BEBEC0',
        },
        background: {
            default: '#191A23',
            paper: '#222330',
        },
        textNav: {
            main: '#fff',
            light: '#dadada',
        },
        darkNav: {
            main: '#282933',
            light: '#333',
        },
    },
};
