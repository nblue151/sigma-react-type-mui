import * as React from 'react';
import { useState } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Typography from '@mui/material/Typography';
import {
    Box,
    Checkbox,
    FormHelperText,
    InputBase,
    InputLabel,
    MenuItem,
    Stack,
    styled,
    TextField,
    useTheme,
} from '@mui/material';
import DownloadIcon from '@mui/icons-material/Download';
import ReCAPTCHA from 'react-google-recaptcha';
import BoxBotomText from '../../customStyle/BoxBottomText';
import { useAppSelector } from '../../state/hooks';

// import CV from '../../assets/file/CV.pdf';
interface PropsModal {
    open: true | false;
    setOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const DialogCustom = styled(Dialog)(({ theme }) => ({
    '.MuiPaper-root': {
        maxWidth: '880px',
        width: '880px',
        margin: '0',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: theme.palette.background.default,
        [theme.breakpoints.up('xs')]: {
            height: '100vh',
            maxHeight: '100vh',
            borderRadius: '0',
            padding: '0 20px',
        },
        [theme.breakpoints.up('sm')]: {
            height: 'auto',
            borderRadius: '4px',
            margin: '0 20px',
            padding: '0 60px',
        },
    },
}));

const Title = styled(Typography)(({ theme }) => ({
    fontSize: '34px',
    fontWeight: '700',
    lineHeight: '34px',
    margin: '33px 0 10px',
    [theme.breakpoints.up('xs')]: {
        fontSize: '24px',
        fontWeight: '700',
        lineHeight: '24px',
        margin: '18px 0 8px',
    },
    [theme.breakpoints.up('sm')]: {
        fontSize: '34px',
        fontWeight: '700',
        lineHeight: '34px',
        margin: '33px 0 10px',
    },
}));

const SubTitle = styled(Typography)(({ theme }) => ({
    fontSize: '16px',
    fontWeight: '400',
    lineHeight: '24px',
}));

const ContentModal = styled(DialogContent)(({ theme }) => ({
    padding: '0',
    width: '100%',
    display: 'grid',
    gap: '0 20px',
    [theme.breakpoints.up('xs')]: {
        gridTemplateColumns: '1fr',
        marginTop: '12px',
    },
    [theme.breakpoints.up('sm')]: {
        gridTemplateColumns: '1fr 1fr',
        marginTop: '46px',
    },
}));

const FormControl = styled(Box)(({ theme }) => ({}));

const InputLabelCustom = styled(InputLabel)(({ theme }) => ({
    fontSize: '16px',
    fontWeight: '24px',
    lineHeight: '24px',
    marginBottom: '5px',

    span: {
        marginLeft: '4px',
        color: theme.palette.error.main,
    },
}));

const InputCustom = styled(InputBase)(({ theme }) => ({
    width: '100%',
    input: {
        width: '100%',
        border: '1px solid #CFD5E1',
        boxSizing: 'border-box',
        padding: '10px 16px',

        '&:hover': {
            borderColor: theme.palette.text.primary,
        },

        '&:focus': {
            borderColor: theme.palette.primary.main,
        },
    },
    [theme.breakpoints.up('xs')]: {
        input: {
            height: '40px',
            borderRadius: '4px',
        },
    },
    [theme.breakpoints.up('sm')]: {
        input: {
            height: '48px',
            borderRadius: '8px',
        },
    },
}));

const SelectCustom = styled(TextField)(({ theme }) => ({
    '& .MuiOutlinedInput-root': {
        '& fieldset': {
            border: '1px solid #CFD5E1',
        },
        '&.Mui-focused fieldset': {
            border: `1px solid ${theme.palette.primary.main}`,
        },
    },
    '> .MuiInputBase-root': {
        borderRadius: '8px',

        '> .MuiSelect-select': {
            padding: '0 16px',
            height: '48px',
            display: 'flex',
            alignItems: 'center',
        },
    },
    [theme.breakpoints.up('xs')]: {
        '> .MuiInputBase-root': {
            borderRadius: '4px',

            '> .MuiSelect-select': {
                height: '40px',
            },
        },
    },
    [theme.breakpoints.up('sm')]: {
        '> .MuiInputBase-root': {
            borderRadius: '8px',

            '> .MuiSelect-select': {
                height: '48px',
            },
        },
    },
}));

const ErrorText = styled(FormHelperText)(({ theme }) => ({
    color: theme.palette.error.main,
    [theme.breakpoints.up('xs')]: {
        input: {
            height: '40px',
        },
    },
    [theme.breakpoints.up('sm')]: {
        input: {
            height: '48px',
        },
    },
}));

const CheckBoxOutline = styled(Box)(({ theme }) => ({
    width: '20px',
    height: '20px',
    border: '1px solid #CFD5E1',
    borderRadius: '3px',
}));

const CheckedWrap = styled(Box)(({ theme }) => ({
    width: '20px',
    height: '20px',
    border: `1px solid ${theme.palette.primary.main}`,
    borderRadius: '3px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

const CheckedCustom = styled(Box)(({ theme }) => ({
    width: '14px',
    height: '14px',
    backgroundColor: theme.palette.primary.main,
    borderRadius: '2px',
}));

const LabelCheckbox = styled(Typography)(({ theme }) => ({
    '& a': {
        marginLeft: '4px',
        textDecoration: 'none',
        color: theme.palette.primary.main,
    },

    '& span': {
        color: theme.palette.error.main,
    },
    [theme.breakpoints.up('xs')]: {
        fontSize: '14px',
    },
    [theme.breakpoints.up('sm')]: {
        fontSize: '16px',
    },
}));

const ButtonSubmit = styled(Button)(({ theme }) => ({
    padding: '7.75px 34px',
    borderRadius: '50px',
    textTransform: 'capitalize',
    '&.Mui-disabled': {
        backgroundColor: theme.palette.primary.main,
        opacity: '0.6',
        color: '#fff',
    },
}));

interface FormData {
    firstName: string;
    lastName: string;
    country: string;
    phone: string;
    company: string;
    email: string;
}

export default function ModalDownload({ open, setOpen }: PropsModal) {
    const theme = useTheme();
    const mode = useAppSelector((state) => state.themeGlobal.mode);

    const [checked, setChecked] = useState(false);
    const [vetifyCAPTCHA, setVetifyCAPTCHA] = useState(false);

    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            country: '',
            phone: '',
            company: '',
            email: '',
        },
        validationSchema: Yup.object({
            firstName: Yup.string().required('Your first name is required!'),
            lastName: Yup.string().required('Your last name is required!'),
            country: Yup.string().required('Your country is required!'),
            company: Yup.string().required('Your company is required!'),
            email: Yup.string()
                .required('Your email is required')
                .matches(
                    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    'Please enter a valid email address'
                ),
            phone: Yup.string()
                .required('Your address is required!')
                .matches(/^(84|0[3|5|7|8|9])+([0-9]{8})\b$/, 'Please invalid phone number'),
        }),
        onSubmit: (values: FormData) => {
            console.log(values);
        },
    });

    const handleClose = () => {
        formik.resetForm();
        setChecked(false);
        setOpen(false);
    };

    const handleDowload = () => {
        // window.open('../../assets/file/CV.pdf', '_self');
    };

    return (
        <DialogCustom open={open} onClose={handleClose}>
            <Title variant="h6">Receive the document:</Title>
            <SubTitle>spilot-datasheet-media-2022.pfd</SubTitle>
            <BoxBotomText />
            <form
                onSubmit={formik.handleSubmit}
                style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}
            >
                <ContentModal>
                    <FormControl>
                        <InputLabelCustom htmlFor="firstName">
                            First Name
                            <Box component="span">*</Box>
                        </InputLabelCustom>
                        <InputCustom
                            id="firstName"
                            aria-describedby="firstName"
                            name="firstName"
                            value={formik.values.firstName}
                            onKeyUp={formik.handleBlur}
                            onChange={formik.handleChange}
                        />
                        <ErrorText id="firstName">{formik.errors.firstName ? formik.errors.firstName : ' '}</ErrorText>
                    </FormControl>
                    <FormControl>
                        <InputLabelCustom htmlFor="country">
                            Country
                            <Box component="span">*</Box>
                        </InputLabelCustom>
                        <SelectCustom
                            select
                            fullWidth
                            name="country"
                            value={formik.values.country}
                            onChange={(e) => formik.setFieldValue('country', e.target.value as string)}
                        >
                            <MenuItem value="AU">Australia</MenuItem>
                            <MenuItem value="IN">China</MenuItem>
                            <MenuItem value="US">USA</MenuItem>
                            <MenuItem value="UK">UK</MenuItem>
                            <MenuItem value="VI">Viet Nam</MenuItem>
                        </SelectCustom>
                        <ErrorText id="my-helper-text">{formik.errors.country ? formik.errors.country : ' '}</ErrorText>
                    </FormControl>
                    <FormControl>
                        <InputLabelCustom htmlFor="lastName">
                            Last Name
                            <Box component="span">*</Box>
                        </InputLabelCustom>
                        <InputCustom
                            id="lastName"
                            aria-describedby="lastName"
                            name="lastName"
                            value={formik.values.lastName}
                            onChange={formik.handleChange}
                        />
                        <ErrorText id="lastName">{formik.errors.lastName ? formik.errors.lastName : ' '}</ErrorText>
                    </FormControl>
                    <FormControl>
                        <InputLabelCustom htmlFor="phone">
                            Phone
                            <Box component="span">*</Box>
                        </InputLabelCustom>
                        <InputCustom
                            id="phone"
                            aria-describedby="phone"
                            name="phone"
                            type="phone"
                            value={formik.values.phone}
                            onChange={formik.handleChange}
                        />
                        <ErrorText id="phone">{formik.errors.phone ? formik.errors.phone : ' '}</ErrorText>
                    </FormControl>
                    <FormControl>
                        <InputLabelCustom htmlFor="company">
                            Company
                            <Box component="span">*</Box>
                        </InputLabelCustom>
                        <InputCustom
                            id="company"
                            aria-describedby="company"
                            name="company"
                            value={formik.values.company}
                            onChange={formik.handleChange}
                        />
                        <ErrorText id="company">{formik.errors.company ? formik.errors.company : ' '}</ErrorText>
                    </FormControl>
                    <FormControl>
                        <InputLabelCustom htmlFor="email">
                            Email
                            <Box component="span">*</Box>
                        </InputLabelCustom>
                        <InputCustom
                            id="email"
                            aria-describedby="email"
                            name="email"
                            type="email"
                            value={formik.values.email}
                            onChange={formik.handleChange}
                            error
                        />
                        <ErrorText id="email">{formik.errors.email ? formik.errors.email : ' '}</ErrorText>
                    </FormControl>
                </ContentModal>
                <Stack direction="row" alignItems="center" mb="20px">
                    <Checkbox
                        sx={{ padding: '0', marginRight: '8px' }}
                        disableRipple
                        checked={checked}
                        onChange={(e) => setChecked(e.target.checked)}
                        icon={<CheckBoxOutline />}
                        checkedIcon={
                            <CheckedWrap>
                                <CheckedCustom />
                            </CheckedWrap>
                        }
                    />
                    <LabelCheckbox variant="body1">
                        By downloading, I agree to sharing my data with Spilot under the terms defined in the
                        <Box component="a" href="">
                            Privacy Policy
                        </Box>
                        <Box component="span">*</Box>
                    </LabelCheckbox>
                </Stack>
                {/* eslint-disable-next-line react/jsx-no-bind */}
                <ReCAPTCHA
                    sitekey="6LdAz1EiAAAAADEU1_gYgBZYvgzzBmWGZ6FHb78B"
                    onChange={(token) => {
                        console.log({ token });
                        setVetifyCAPTCHA(true);
                    }}
                    theme={mode === 'light' ? 'light' : 'dark'}
                />
                <DialogActions sx={{ margin: '20px 0 26px', padding: '0' }}>
                    <IconButton
                        onClick={handleClose}
                        sx={{
                            position: 'absolute',
                            top: '4px',
                            right: '8px',
                            '&:hover': { backgroundColor: 'transparent' },
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                    <Stack
                        direction="row"
                        alignItems="center"
                        sx={{ borderRadius: '50px', backgroundColor: theme.palette.background.paper }}
                    >
                        <Typography variant="body1" sx={{ display: { xs: 'none', sm: 'block' }, padding: '0 25px' }}>
                            spilot-datasheet-media-2022.pfd
                        </Typography>
                        <ButtonSubmit
                            disabled={!(checked && vetifyCAPTCHA)}
                            type="submit"
                            variant="contained"
                            endIcon={<DownloadIcon />}
                            onClick={handleDowload}
                        >
                            Download
                        </ButtonSubmit>
                    </Stack>
                </DialogActions>
            </form>
        </DialogCustom>
    );
}
