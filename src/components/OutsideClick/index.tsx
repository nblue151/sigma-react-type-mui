import { Box } from '@mui/material';
import React, { useRef, useEffect } from 'react';

interface Props {
    children: React.ReactElement;
    handleClicked: React.Dispatch<React.SetStateAction<boolean>>;
}

// Custom click outside
export default function OutsideClick({ children, handleClicked }: Props) {
    const ref = useRef<HTMLDivElement | null>(null);

    useEffect(() => {
        const handleClickOutside = (event: any) => {
            if (ref.current && !ref.current.contains(event.target)) {
                handleClicked(false);
            }
        };

        // Bind the event listener
        document.addEventListener('mousedown', handleClickOutside);

        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [ref]);

    return <div ref={ref}>{children}</div>;
}
