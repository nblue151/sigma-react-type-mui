import { Stack, Box, Typography, useTheme, Button } from '@mui/material';
import React from 'react';
import ModalDownload from '../ModalDownload';

interface BannerProps {
    img: string;
    shadowImg: true | false;
    title: string;
    subTitle: string;
}

function Banner({ img, shadowImg, title, subTitle }: BannerProps) {
    const theme = useTheme();

    const [open, setOpen] = React.useState(false);

    return (
        <div>
            <Box
                sx={{
                    margin: { xs: '0 8px', md: '0 20px', lg: '0 25px' },
                    height: { xs: '270px', sm: '400px', md: '500px' },
                    background: shadowImg
                        ? `linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${img});`
                        : `url(${img});`,
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition: 'center',
                    borderRadius: '12px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'column',
                    position: 'relative',
                }}
            >
                <Typography
                    variant="h2"
                    sx={{
                        width: { lg: '780px' },
                        fontWeight: '700',
                        fontSize: { xs: '26px', md: '40px', lg: '48px' },
                        lineHeight: { xs: '32px', md: '48px', lg: '56px' },
                        padding: { xs: '0 15px', sm: '0 50px', lg: '0' },
                        marginTop: { xs: '40px', sm: '0' },
                        color: '#fff',
                        textAlign: 'center',
                    }}
                >
                    Deliver a great experience for your customers
                </Typography>

                <Button
                    variant="contained"
                    sx={{
                        width: { xs: '150px', md: '200px', lg: '238px' },
                        height: { xs: '32px', md: '40px', lg: '48px' },
                        borderRadius: '50px',
                        marginTop: { xs: '10px', sm: '30px' },

                        textSize: '18px',
                        lineHeight: '32px',
                        fontWeight: '600',
                        textTransform: 'capitalize',
                        color: '#fff',
                        background: 'linear-gradient(270deg, #2CFDDC 0%, #A35FF8 108.78%);',
                    }}
                    onClick={() => setOpen(true)}
                >
                    Download
                </Button>

                <Stack
                    direction="column"
                    alignItems="center"
                    justifyContent="center"
                    sx={{
                        display: { xs: 'none', lg: 'flex' },
                        width: { xs: '80%', lg: '1200px' },
                        height: { xs: '100px', md: '120px', lg: '140px' },
                        borderRadius: '20px',
                        position: 'absolute',
                        bottom: '0',
                        left: '50%',
                        transform: 'translate(-50%, 50%)',
                        backgroundColor: { xs: theme.palette.background.paper, lg: theme.palette.background.default },
                    }}
                >
                    <Typography
                        variant="h3"
                        sx={{
                            fontSize: { xs: '32px', md: '40px', lg: '48px' },
                            lineHeight: { xs: '54px', md: '62px', lg: '70px' },
                            letterSpacing: '-0.7px',
                            fontWeight: 'bold',
                            color: theme.palette.text.primary,
                        }}
                    >
                        {title}
                    </Typography>
                    <Typography
                        variant="h6"
                        sx={{
                            fontSize: { xs: '16px', md: '18px', lg: '20px' },
                            lineHeight: { xs: '26px', md: '28px', lg: '30px' },
                            fontWeight: '400',
                            color: theme.palette.text.secondary,
                        }}
                    >
                        {subTitle}
                    </Typography>
                </Stack>
            </Box>
            <ModalDownload open={open} setOpen={setOpen} />
        </div>
    );
}

export default Banner;
