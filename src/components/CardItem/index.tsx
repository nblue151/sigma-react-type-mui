import { Card, CardActions, CardContent, CardMedia, Typography, useTheme } from '@mui/material';
import React from 'react';
import { Link } from 'react-router-dom';

interface Data {
    url?: string;
    dateTime?: string;
    text?: string;
}
interface CardItemProps {
    data: Data;
}

function CardItem({ data }: CardItemProps) {
    const theme = useTheme();
    return (
        <Card
            sx={{
                borderRadius: '12px',
                boxShadow: '0px 0px 1px rgba(40, 41, 61, 0.04), 0px 2px 4px rgba(96, 97, 112, 0.16)',
            }}
        >
            <CardMedia component="img" height="200px" image={data.url} alt="" sx={{}} />
            <CardContent sx={{ padding: '20px 50px 0 24px' }}>
                <Typography
                    gutterBottom
                    variant="h5"
                    component="div"
                    sx={{
                        fontSize: '12px',
                        fontWeight: '400',
                        lineHeight: '20px',
                        color: theme.palette.text.secondary,
                    }}
                >
                    {data.dateTime}
                </Typography>
                <Typography
                    variant="body2"
                    sx={{
                        fontSize: '20px',
                        fontWeight: '400',
                        lineHeight: '30px',
                        color: theme.palette.text.secondary,
                    }}
                >
                    {data.text}
                </Typography>
            </CardContent>
            <CardActions sx={{ padding: '0 24px 20px' }}>
                <Link to="" style={{ textDecoration: 'none' }}>
                    <Typography
                        variant="body2"
                        sx={{
                            fontSize: '18px',
                            fontWeight: '600',
                            lineHeight: '32px',
                            color: theme.palette.primary.main,
                            '&:hover': {
                                color: theme.palette.primary.light,
                            },
                        }}
                    >
                        Đọc thêm
                    </Typography>
                </Link>
            </CardActions>
        </Card>
    );
}

export default CardItem;
