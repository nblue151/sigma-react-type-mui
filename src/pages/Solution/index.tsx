/* eslint-disable react/no-array-index-key */
import { Box, Stack, styled, Typography, useTheme, Paper } from '@mui/material';
import React from 'react';
import images from '../../assets/images';
import Banner from '../../components/Banner';
import CardItem from '../../components/CardItem';
import { solution1, solution2, solution3 } from '../../assets/data';
import BoxBotomText from '../../customStyle/BoxBottomText';

const SubContent = styled(Typography)(({ theme }) => ({
    fontSize: '16px',
    fontWeight: '300',
    color: theme.palette.text.secondary,
    [theme.breakpoints.up('xs')]: {
        lineHeight: '30px',
    },
    [theme.breakpoints.up('md')]: {
        lineHeight: '40px',
    },
    [theme.breakpoints.up('lg')]: {
        lineHeight: '50px',
    },
}));

const StackSolution = styled(Stack)(({ theme }) => ({
    [theme.breakpoints.up('xs')]: {
        width: '100%',
        margin: '48px auto 30px',
        padding: '0 8px',
    },
    [theme.breakpoints.up('md')]: {
        padding: '0 20px',
    },
    [theme.breakpoints.up('lg')]: {
        width: '1200px',
        margin: '70px auto 60px',
    },
}));

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: 'transparent',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
}));

const GridCustom = styled(Box)(({ theme }) => ({
    display: 'grid',
    gap: '32px',
    [theme.breakpoints.up('xs')]: {
        gridTemplateColumns: '1fr',
        margin: '0 8px 100px',
    },
    [theme.breakpoints.up('sm')]: {
        gridTemplateColumns: '1fr 1fr',
        margin: '0 20px 100px',
    },
    [theme.breakpoints.up('lg')]: {
        gridTemplateColumns: '1fr 1fr 1fr',
        margin: '0 auto 100px',
    },
}));

function Solution() {
    const theme = useTheme();

    return (
        <Box>
            <Banner
                img={images.banner}
                shadowImg
                title="We are Sigma OTT"
                subTitle="The world leading of video streaming solution"
            />

            <StackSolution direction="column">
                {solution1.map((item, index) => (
                    <Stack
                        key={index}
                        direction={{
                            xs: 'column',
                            sm: index % 2 === 0 ? 'row' : 'row-reverse',
                        }}
                        justifyContent={index % 2 === 0 ? 'space-between' : 'start'}
                        spacing={{
                            xs: '0',
                            sm: index % 2 === 0 ? '0' : '45px',
                        }}
                        alignItems={{ xs: 'start', sm: 'center' }}
                        mb="48px"
                        mx="0 12px"
                    >
                        <Box mb={{ xs: '10px', sm: '0' }}>
                            <Typography
                                variant="h6"
                                sx={{
                                    fontSize: { xs: '24px', md: '30px', lg: '34px' },
                                    lineHeight: { xs: '30px', md: '36px', lg: '40px' },
                                    fontWeight: '700',
                                    color: theme.palette.text.primary,
                                }}
                            >
                                {item.title}
                            </Typography>
                            <BoxBotomText />

                            <Stack direction="column" mt={{ xs: '12px', sm: '28px' }}>
                                <SubContent variant="body2">
                                    Tăng mức độ phủ sóng theo khu vực / Địa lý / ISP
                                </SubContent>
                                <SubContent variant="body2">Cải thiện hiệu suất tổng thể</SubContent>
                                <SubContent variant="body2">Dự phòng / Chuyển đổi dự phòng</SubContent>
                                <SubContent variant="body2">
                                    Giúp hệ thống hoạt động tốt, đáp ứng lượng CCU lớn.
                                </SubContent>
                            </Stack>
                        </Box>
                        <Box
                            component="img"
                            alt="img"
                            src={item.img}
                            sx={{
                                width: { xs: '100%', sm: '329px', md: '429px', lg: '529px' },
                                objectFit: 'cover',
                            }}
                        />
                    </Stack>
                ))}
            </StackSolution>

            <Box sx={{ backgroundColor: theme.palette.background.paper }}>
                <Stack direction="column" justifyContent="center" width={{ sx: '100%', lg: '1200px' }} m="0 auto">
                    <Typography
                        variant="h5"
                        sx={{
                            fontSize: '32px',
                            lineHeight: { xs: '36px', md: '48px' },
                            fontWeight: '700',
                            letterSpacing: '-0.4px',
                            color: theme.palette.text.primary,
                            textAlign: 'center',
                            marginTop: { xs: '38px', md: '48px', lg: '68px' },
                        }}
                    >
                        Headline is a beatiful way to begin dreaming
                    </Typography>
                    <Typography
                        variant="subtitle1"
                        sx={{
                            fontSize: '20px',
                            lineHeight: '48px',
                            fontWeight: '400',
                            letterSpacing: '-0.4px',
                            color: theme.palette.text.secondary,
                            textAlign: 'center',
                            marginBottom: { xs: '38px', md: '48px', lg: '70px' },
                        }}
                    >
                        Headline is a beatiful way to begin dreaming
                    </Typography>
                    <GridCustom>
                        {solution2.map((data, index) => (
                            <Item key={index} elevation={0}>
                                <Box component="img" alt="" src={data.url} />
                                <Typography
                                    variant="subtitle1"
                                    sx={{
                                        fontSize: '24px',
                                        lineHeight: '48px',
                                        fontWeight: '700',
                                        letterSpacing: '-0.4px',
                                        color: theme.palette.text.primary,
                                        marginTop: '28px',
                                    }}
                                >
                                    {data.title}
                                </Typography>
                                <Typography
                                    variant="subtitle2"
                                    sx={{
                                        fontSize: '20px',
                                        lineHeight: '23px',
                                        fontWeight: '400',
                                        textAlign: 'center',
                                        color: theme.palette.text.secondary,
                                        padding: '0 50px',
                                    }}
                                >
                                    {data.text}
                                </Typography>
                            </Item>
                        ))}
                    </GridCustom>
                </Stack>
            </Box>

            <Box>
                <Typography
                    variant="h5"
                    sx={{
                        fontSize: '32px',
                        lineHeight: { xs: '36px', md: '48px' },
                        fontWeight: '700',
                        letterSpacing: '-0.4px',
                        color: theme.palette.text.primary,
                        textAlign: 'center',
                        marginTop: { xs: '38px', md: '48px', lg: '68px' },
                    }}
                >
                    Latest Updates
                </Typography>
                <Typography
                    variant="subtitle1"
                    sx={{
                        fontSize: '20px',
                        lineHeight: '48px',
                        fontWeight: '400',
                        letterSpacing: '-0.4px',
                        color: theme.palette.text.secondary,
                        textAlign: 'center',
                        marginBottom: { xs: '38px', md: '48px', lg: '68px' },
                    }}
                >
                    See what the press has to say about us
                </Typography>
                <GridCustom width={{ sx: '100%', lg: '1200px' }}>
                    {solution3.map((data, index) => (
                        <CardItem key={index} data={data} />
                    ))}
                </GridCustom>
            </Box>
        </Box>
    );
}

export default Solution;
