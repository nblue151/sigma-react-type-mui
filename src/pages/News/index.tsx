import React from 'react';
import { Box, styled } from '@mui/material';

import { news } from '../../assets/data';
import images from '../../assets/images';
import Banner from '../../components/Banner';
import CardItem from '../../components/CardItem';

const GridCustom = styled(Box)(({ theme }) => ({
    display: 'grid',
    [theme.breakpoints.up('xs')]: {
        gridTemplateColumns: '1fr',
        margin: '40px 8px 100px',
    },
    [theme.breakpoints.up('sm')]: {
        gridTemplateColumns: '1fr 1fr',
        margin: '40px 20px 100px',
    },
    [theme.breakpoints.up('lg')]: {
        gridTemplateColumns: '1fr 1fr 1fr',
        margin: '80px auto 100px',
    },
    gap: '32px',
}));

function New() {
    return (
        <div>
            <Banner
                img={images.banner2}
                shadowImg={false}
                title="Latest Updates"
                subTitle="See what the press has to say about us"
            />

            <GridCustom width={{ sx: '100%', lg: '1200px' }}>
                {news.map((data, index) => (
                    // eslint-disable-next-line react/no-array-index-key
                    <CardItem key={index} data={data} />
                ))}
            </GridCustom>
        </div>
    );
}

export default New;
