import { Box, Button, Stack, styled, TextField, Typography, useTheme } from '@mui/material';
import React from 'react';
import images from '../../assets/images';
import BoxBotomText from '../../customStyle/BoxBottomText';
import { useAppSelector } from '../../state/hooks';

const BoxWrap = styled(Box)(({ theme }) => ({
    margin: '100px auto',
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    alignItems: 'start',
    justifyContent: 'space-between',
    [theme.breakpoints.up('xs')]: {
        height: 'auto',
        width: '100%',
        padding: '0 20px',
        flexDirection: 'column',
    },
    [theme.breakpoints.up('md')]: {
        height: '550px',
        padding: '0 40px',
        flexDirection: 'row',
    },
    [theme.breakpoints.up('lg')]: {
        width: '1200px',
        padding: '0 80px',
        borderRadius: '20px',
    },
}));

const TextFieldCustom = styled(TextField)(({ theme }) => ({
    width: '100%',
    marginBottom: '14px',
}));

function Contact() {
    const theme = useTheme();

    const mode = useAppSelector((state) => state.themeGlobal.mode);
    return (
        <Box
            sx={
                mode === 'light'
                    ? {
                          margin: { xs: '0 8px 700px', md: '0 8px 700px', lg: '0 25px 300px' },
                          height: { xs: '270px', sm: '400px', md: '500px' },
                          background: `url(${images.banner3});`,
                          backgroundSize: 'cover',
                          backgroundRepeat: 'no-repeat',
                          backgroundPosition: 'center',
                          borderRadius: '12px',
                          display: 'flex',
                          justifyContent: 'center',
                          position: 'relative',
                      }
                    : {}
            }
        >
            <BoxWrap
                sx={
                    mode === 'light'
                        ? {
                              marginTop: '0',
                              position: 'absolute',
                              top: { xs: '258px', sm: '388px', lg: '100px' },
                              left: '50%',
                              transform: 'translateX(-50%)',
                              borderRadius: '0 0 12px 12px',
                          }
                        : {}
                }
            >
                <Box sx={{ flex: 1 }} mr={{ xs: '0', md: '20px' }}>
                    <Typography
                        variant="h3"
                        sx={{
                            margin: { xs: '30px 0 8px', md: '130px 0 8px' },
                            fontSize: '32px',
                            lineHeight: '48px',
                            fontWeight: '700',
                            letterSpacing: '-0.4px',
                            color: theme.palette.text.primary,
                        }}
                    >
                        Contact
                    </Typography>
                    <BoxBotomText />
                    <Typography
                        variant="body2"
                        sx={{
                            marginTop: '26px',
                            maxWidth: { xs: '100%', md: '560px' },
                            fontSize: '20px',
                            lineHeight: '30px',
                            fontWeight: '300',
                            color: theme.palette.text.secondary,
                        }}
                    >
                        I think it takes more than three seconds to build your brand, and consumers are willing to spend
                        the time if your content is compelling, relevant, and valuable
                    </Typography>
                </Box>
                <Stack direction="column" width={{ xs: '100%', md: '310px' }}>
                    <form style={{ width: '100%' }}>
                        <Typography
                            variant="body2"
                            component="div"
                            sx={{
                                marginTop: { xs: '50px', md: '100px' },
                                marginBottom: '10px',
                                fontSize: '14px',
                                fontWeight: '400',
                                lineHeight: '24px',
                                color: theme.palette.text.secondary,
                            }}
                        >
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry
                        </Typography>
                        <TextFieldCustom label="Company Name" id="margin-normal" name="company" size="small" />
                        <TextFieldCustom label="Email" id="margin-normal" name="email" size="small" />
                        <Stack direction={{ xs: 'column', sm: 'row' }} gap={{ xs: '0', sm: '14px' }}>
                            <TextFieldCustom label="First Name" id="margin-normal" name="firstName" size="small" />
                            <TextFieldCustom label="Last Name" id="margin-normal" name="lastName" size="small" />
                        </Stack>
                        <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            sx={{
                                width: '100%',
                                height: '40px',
                                textTransform: 'capitalize',
                                marginBottom: { xs: '50px', md: '0' },
                            }}
                        >
                            Send
                        </Button>
                    </form>
                </Stack>
            </BoxWrap>
        </Box>
    );
}

export default Contact;
