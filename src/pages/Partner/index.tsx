/* eslint-disable react/no-array-index-key */
import { Box, Typography, Stack, useTheme, styled } from '@mui/material';
import React from 'react';
import { partner } from '../../assets/data';
import images from '../../assets/images';

const GridCustom = styled(Box)(({ theme }) => ({
    display: 'grid',
    gridTemplateColumns: '1fr 1fr 1fr',
    gap: '16px 30px',
    width: '100%',
    [theme.breakpoints.up('xs')]: {
        gridTemplateColumns: '1fr',
        marginTop: '40px',
    },
    [theme.breakpoints.up('sm')]: {
        gridTemplateColumns: '1fr 1fr',
        margin: '60px',
    },
    [theme.breakpoints.up('lg')]: {
        gridTemplateColumns: '1fr 1fr 1fr',
        margin: '60px',
    },
}));

function Partner() {
    const theme = useTheme();

    return (
        <div>
            <Box
                sx={{
                    margin: { xs: '0 8px 2000px', md: '0 8px 1300px', lg: '0 25px 1300px' },
                    height: { xs: '270px', sm: '400px', md: '500px' },
                    background: `url(${images.banner3});`,
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition: 'center',
                    borderRadius: '12px',
                    display: 'flex',
                    justifyContent: 'center',
                    position: 'relative',
                }}
            >
                <Typography
                    variant="h2"
                    sx={{
                        textAlign: 'center',
                        marginTop: '106px',
                        fontWeight: '700',
                        fontSize: '48px',
                        lineHeight: '56px',
                        color: '#fff',
                    }}
                >
                    Partner
                </Typography>

                <Stack
                    direction="column"
                    alignItems="center"
                    justifyContent="center"
                    sx={{
                        width: { xs: '100%', lg: '1200px' },
                        padding: { xs: '20px', md: '20px 30px 50px', lg: '34px 45px 106px' },
                        borderRadius: '20px',
                        position: 'absolute',
                        top: { xs: '110%', lg: '244px' },
                        left: '50%',
                        transform: 'translateX(-50%)',
                        backgroundColor: theme.palette.background.paper,
                    }}
                >
                    <Typography
                        variant="h3"
                        sx={{
                            fontSize: { xs: '26px', md: '28px', lg: '32px' },
                            lineHeight: { xs: '32px', md: '40px', lg: '48px' },
                            letterSpacing: '-0.4px',
                            fontWeight: '700',
                            textAlign: 'center',
                            color: theme.palette.text.primary,
                        }}
                    >
                        Technologies and work with leading ecosystem partners
                    </Typography>
                    <Typography
                        variant="h6"
                        sx={{
                            fontSize: '20px',
                            lineHeight: { xs: '32px', md: '40px', lg: '48px' },
                            fontWeight: '400',
                            textAlign: 'center',
                            color: theme.palette.text.secondary,
                        }}
                    >
                        See what the press has to say about us
                    </Typography>

                    <GridCustom>
                        {partner.map((item, index) => (
                            <Box
                                key={index}
                                component="img"
                                alt=""
                                src={item}
                                sx={{
                                    width: '100%',
                                    objectFit: 'cover',
                                    borderRadius: '6px',
                                }}
                            />
                        ))}
                    </GridCustom>
                </Stack>
            </Box>
        </div>
    );
}

export default Partner;
