import { configureStore } from '@reduxjs/toolkit';
import themeGlobal from './sliceGlobal/theme';

const store = configureStore({
    reducer: {
        themeGlobal: themeGlobal.reducer,
    },
});

export default store;
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
