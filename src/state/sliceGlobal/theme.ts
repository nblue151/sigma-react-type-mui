/* eslint-disable no-param-reassign */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface Theme {
    mode: string;
    language: string;
}

// Get theme from localstorage
const themeLocal = localStorage.getItem('theme');
let theme: Theme;
if (themeLocal !== null) {
    theme = JSON.parse(themeLocal);
} else {
    theme = { mode: 'light', language: 'vietnam' };
}
const initialState: Theme = {
    mode: theme.mode,
    language: theme.language,
};

export default createSlice({
    name: 'theme',
    initialState,
    reducers: {
        toggleMode: (state, action: PayloadAction<string>) => {
            state.mode = action.payload;
            const themeSave = JSON.stringify(state);
            localStorage.setItem('theme', themeSave);
        },
        changeLanguage: (state, action: PayloadAction<string>) => {
            state.language = action.payload;
            const themeSave = JSON.stringify(state);
            localStorage.setItem('theme', themeSave);
        },
    },
});
