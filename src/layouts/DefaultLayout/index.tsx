import React from 'react';
import { Box } from '@mui/material';
import Footer from '../components/Footer';
import Header from '../components/Header';
import { useAppSelector } from '../../state/hooks';

type Props = {
    // eslint-disable-next-line react/require-default-props
    children?: React.ReactElement;
};

function DefaultLayout({ children }: Props) {
    const mode = useAppSelector((state) => state.themeGlobal.mode);
    return (
        <Box>
            <Header />
            <div style={{ marginTop: mode === 'dark' ? '64px' : '23px' }}>{children}</div>
            <Footer />
        </Box>
    );
}

export default DefaultLayout;
