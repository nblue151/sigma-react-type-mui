import React from 'react';
import { Box, Stack, styled, Tooltip, Typography, useTheme } from '@mui/material';
import FacebookOutlinedIcon from '@mui/icons-material/FacebookOutlined';
import GitHubIcon from '@mui/icons-material/GitHub';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import { Link } from 'react-router-dom';
import images from '../../../assets/images';

const TypographyCustom = styled(Typography)(({ theme }) => ({
    fontSize: '14px',
    fontWeight: '400',
    lineHeight: '24px',
    color: theme.palette.textNav.main,
}));

const SocialBox = styled(Box)(({ theme }) => ({
    width: '44px',
    height: '44px',
    borderRadius: '50px',
    backgroundColor: theme.palette.background.default,
    color: theme.palette.text.primary,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

function Footer() {
    const theme = useTheme();
    return (
        <Box sx={{ backgroundColor: theme.palette.darkNav.main }} px={{ xs: '20px', lg: '0' }}>
            <Box sx={{ width: { xs: '100%', lg: '1200px' }, margin: '0 auto' }}>
                <Stack
                    direction={{ xs: 'column', sm: 'row' }}
                    justifyContent="space-between"
                    sx={{
                        padding: { xs: '20px 0', sm: '40px 0 20px', md: '70px 0 20px' },
                        borderBottom: `1px solid ${theme.palette.textNav.main}`,
                    }}
                >
                    <Stack direction="row" justifyContent="space-between" sx={{ gap: '70px' }}>
                        <TypographyCustom variant="body2">Solution</TypographyCustom>
                        <TypographyCustom variant="body2">News</TypographyCustom>
                    </Stack>
                    <Stack direction="row" justifyContent="space-between" sx={{ gap: '70px' }}>
                        <TypographyCustom variant="body2">Partner</TypographyCustom>
                        <TypographyCustom variant="body2">Contact</TypographyCustom>
                    </Stack>
                </Stack>
                <Stack direction="column" alignItems="center" sx={{ position: 'relative', padding: '48px 0 42px' }}>
                    <Stack direction="row" justifyContent="center" sx={{ gap: '20px' }}>
                        <Link to="/">
                            <Tooltip title="Facebook" arrow placement="top">
                                <SocialBox>
                                    <FacebookOutlinedIcon />
                                </SocialBox>
                            </Tooltip>
                        </Link>
                        <Link to="/">
                            <Tooltip title="Twitter" arrow placement="top">
                                <SocialBox>
                                    <TwitterIcon />
                                </SocialBox>
                            </Tooltip>
                        </Link>
                        <Link to="/">
                            <Tooltip title="Github" arrow placement="top">
                                <SocialBox>
                                    <GitHubIcon />
                                </SocialBox>
                            </Tooltip>
                        </Link>
                        <Link to="/">
                            <Tooltip title="Instagram" arrow placement="top">
                                <SocialBox>
                                    <InstagramIcon />
                                </SocialBox>
                            </Tooltip>
                        </Link>
                    </Stack>
                    <Typography
                        variant="body2"
                        sx={{
                            marginTop: '26px',
                            fontSize: '15px',
                            lineHeight: '24px',
                            fontWeight: '400',
                            letterSpacing: '-0.015em',
                            color: theme.palette.textNav.main,
                            opacity: '0.8',
                        }}
                    >
                        © Sigma, Inc. 2022. We love our users!
                    </Typography>
                    <Box
                        display={{ xs: 'none', md: 'block' }}
                        component="img"
                        alt=""
                        src={images.lightLogo}
                        height="34px"
                        sx={{ position: 'absolute', left: '0', top: '50%', transfrom: 'translateY(-50%)' }}
                    />
                </Stack>
            </Box>
        </Box>
    );
}

export default Footer;
