/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import {
    AppBar,
    Box,
    IconButton,
    keyframes,
    Stack,
    styled,
    Toolbar,
    Typography,
    InputBase,
    useScrollTrigger,
    useTheme,
    InputAdornment,
    Drawer,
    FormControlLabel,
    Switch,
    Accordion,
    AccordionSummary,
    AccordionDetails,
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import LightModeIcon from '@mui/icons-material/LightMode';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import HomeIcon from '@mui/icons-material/Home';
import FeedIcon from '@mui/icons-material/Feed';
import GroupWorkIcon from '@mui/icons-material/GroupWork';
import LanguageIcon from '@mui/icons-material/Language';
import PermContactCalendarIcon from '@mui/icons-material/PermContactCalendar';

import { useResizeDetector } from 'react-resize-detector';
import { Link, NavLink, useNavigate } from 'react-router-dom';

import images from '../../../assets/images';
import { useAppDispatch, useAppSelector } from '../../../state/hooks';
import themeSlice from '../../../state/sliceGlobal/theme';
import OutsideClick from '../../../components/OutsideClick';

// Custom style
const NavbarTypo = styled(Typography)(({ theme }) => ({
    paddingLeft: '8px',
    lineHeight: '40px',
    textTransform: 'capitalize',
    backgroundColor: 'transparent',
    whiteSpace: 'nowrap',
}));

const transformToTop = keyframes`
    from {
        top: 100%
    }
    to {
        top: 80%
    }
`;

const NavbarHaveSubNav = styled(Stack)(({ theme }) => ({
    flexDirection: 'row',
    alignItems: 'center',
    borderTopLeftRadius: '6px',
    borderTopRightRadius: '6px',
    color: theme.palette.textNav.main,
    cursor: 'pointer',
    position: 'relative',

    '&:hover': {
        backgroundColor: theme.palette.darkNav.main,
    },
    '&:hover .MuiBox-root': {
        display: 'block',
        animation: `${transformToTop} 0.5s ease`,
    },
    '&:hover .MuiSvgIcon-root': {
        transform: 'rotate(-180deg)',
    },
}));

const MenuCustom = styled(Box)(({ theme }) => ({
    display: 'none',
    position: 'absolute',
    top: '80%',
    left: '0',
    backgroundColor: theme.palette.darkNav.main,
    textAlign: 'start',
    borderRadius: '0 6px 6px 6px',
    padding: '6px 0',
    minWidth: '100%',

    '.css-1d9cypr-MuiStack-root:hover': {
        backgroundColor: theme.palette.darkNav.light,
    },
}));

const TypographySubNav = styled(Typography)(({ theme }) => ({
    color: theme.palette.textNav.light,
    padding: '6px 8px',
    fontSize: '14px',
    '&:hover': { backgroundColor: theme.palette.darkNav.light },
}));

const InputSearch = styled(InputBase)(({ theme }) => ({
    color: '#333',
    fontSize: '14px',
    padding: '0 10px',
    borderRadius: '50px',
    // backgroundColor: '#fff',

    input: {
        padding: '6px 0',
        width: '0',
        transition: 'all 0.5s ease-in',
    },
    '.MuiSvgIcon-root': {
        color: '#fff',
    },
}));

// Menu Sidbar
const AccordionCustom = styled(Accordion)(({ theme }) => ({
    boxShadow: 'none',
    backgroundColor: 'transparent',
    borderBottom: '1px solid rgba(255, 255, 255, 0.03)',
    borderRadius: '0 !important',
    margin: '0 !important',
    '&::before': { height: 0 },
    '.MuiAccordionSummary-root': {
        minHeight: '40px !important',

        '.MuiSvgIcon-root': {
            marginRight: '8px',
        },

        '.MuiAccordionSummary-content': {
            alignItems: 'center',
            margin: '0',
        },
    },
    '.MuiAccordionDetails-root ': {
        padding: '0',
        div: {
            cursor: 'pointer',
            padding: '6px 0px 6px 48px',
            '&:hover': { backgroundColor: theme.palette.background.paper },
        },
        img: {
            height: '18px',
            width: '18px',
            marginRight: '8px',
        },
    },
}));

const SideBarLink = styled(Stack)(({ theme }) => ({
    cursor: 'pointer',
    alignItems: 'center',
    backgroundColor: 'transparent',
    paddingLeft: '16px',
    borderBottom: '1px solid rgba(255, 255, 255, 0.03)',
    backgroundImage: 'linear-gradient(rgba(255, 255, 255, 0.05), rgba(255, 255, 255, 0.05))',

    '&:hover': { backgroundColor: theme.palette.background.paper },

    ' .MuiSvgIcon-root': {
        marginRight: '8px',
    },

    ' .MuiTypography-root': {
        padding: '8px 0',
    },
}));

// Check scroll navbar
interface Props {
    window?: () => Window;
    children: React.ReactElement;
    thresholdNumber?: number;
    setTrigger: React.Dispatch<React.SetStateAction<boolean>>;
}

function ElevationScroll({ window, children, thresholdNumber, setTrigger }: Props) {
    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: thresholdNumber, // Check position of scroll head
        target: window ? window() : undefined,
    });
    setTrigger(trigger);

    // return <Box>{children}</Box>;
    return React.cloneElement(children);
}

function Header() {
    const theme = useTheme();
    const navigate = useNavigate();
    const { width, ref } = useResizeDetector();

    const [trigger, setTrigger] = useState(false);
    const [showSearch, setShowSearch] = useState(false);
    const [showDrawer, setShowDrawer] = useState(false);
    const [expanded, setExpanded] = useState<string | false>(false);

    // Use redux toolkit
    const dispatch = useAppDispatch();
    const mode = useAppSelector((state) => state.themeGlobal.mode);
    const languageTheme = useAppSelector((state) => state.themeGlobal.language);
    const languageLogo =
        languageTheme !== 'vietnam'
            ? languageTheme === 'english'
                ? images.ukLanguage
                : images.franceLanguage
            : images.vnLanguage;

    // Handle action
    const handleToggleMode = () => {
        dispatch(themeSlice.actions.toggleMode(mode === 'light' ? 'dark' : 'light'));
    };

    const handleLanguage = (language: string) => {
        if (language !== languageTheme) {
            dispatch(themeSlice.actions.changeLanguage(language));
        }
    };

    const handleClickSearchIcon = () => {
        setShowSearch(true);
    };

    const handleExplanded = (isExpanded: boolean, panel: string) => {
        setExpanded(isExpanded ? panel : false);
    };

    const handleResetDrawer = () => {
        setExpanded(false);
        setShowDrawer(false);
    };

    return (
        <ElevationScroll thresholdNumber={mode === 'light' ? 23 : 0} setTrigger={setTrigger}>
            <AppBar
                color="transparent"
                elevation={0}
                sx={
                    mode === 'light'
                        ? trigger
                            ? {
                                  position: 'fixed',
                                  top: '0',
                                  backgroundColor: 'rgba(0, 0, 0, 0.7)',
                              }
                            : {
                                  position: 'absolute',
                                  top: '23px',
                              }
                        : {
                              position: 'fixed',
                              top: '0',
                              backgroundColor: trigger ? 'rgba(0, 0, 0, 0.7)' : 'transparent',
                          }
                }
            >
                <Toolbar sx={{ margin: { xs: '0', lg: '0 120px' } }}>
                    <Box sx={{ flexGrow: 1 }}>
                        <Link to="/" style={{ display: 'flex' }}>
                            <Box
                                component="img"
                                alt="logo"
                                src={mode === 'light' ? images.lightLogo : images.darkLogo}
                                width="120px"
                                height="40px"
                            />
                        </Link>
                    </Box>
                    <Stack direction="row" spacing={4} display={{ xs: 'none', md: 'flex' }}>
                        <NavbarHaveSubNav>
                            <NavbarTypo variant="body2">Solution</NavbarTypo>
                            <KeyboardArrowDownIcon />
                            <MenuCustom sx={{ right: '-43px' }}>
                                <NavLink to="/" style={{ textDecoration: 'none' }}>
                                    <TypographySubNav>Video Processing</TypographySubNav>
                                </NavLink>
                                <NavLink to="/" style={{ textDecoration: 'none' }}>
                                    <TypographySubNav>Security</TypographySubNav>
                                </NavLink>
                                <NavLink to="/" style={{ textDecoration: 'none' }}>
                                    <TypographySubNav>OOT</TypographySubNav>
                                </NavLink>
                            </MenuCustom>
                        </NavbarHaveSubNav>
                        <NavLink
                            to="/news"
                            style={({ isActive }) =>
                                isActive
                                    ? { textDecoration: 'none', color: theme.palette.primary.light }
                                    : { textDecoration: 'none', color: theme.palette.textNav.main }
                            }
                        >
                            <NavbarTypo variant="body2">News</NavbarTypo>
                        </NavLink>
                        <NavLink
                            to="/partner"
                            style={({ isActive }) =>
                                isActive
                                    ? { textDecoration: 'none', color: theme.palette.primary.light }
                                    : { textDecoration: 'none', color: theme.palette.textNav.main }
                            }
                        >
                            <NavbarTypo variant="body2">Partner</NavbarTypo>
                        </NavLink>
                        <NavLink
                            to="/contact"
                            style={({ isActive }) =>
                                isActive
                                    ? { textDecoration: 'none', color: theme.palette.primary.light }
                                    : { textDecoration: 'none', color: theme.palette.textNav.main }
                            }
                        >
                            <NavbarTypo variant="body2">Contact</NavbarTypo>
                        </NavLink>
                        <NavbarHaveSubNav>
                            <NavbarTypo variant="body2">Language</NavbarTypo>
                            <Box component="img" src={languageLogo} alt="vn" ml="4px" width="18px" height="18px" />
                            <KeyboardArrowDownIcon />
                            <MenuCustom sx={{ borderRadius: '0 0 6px 6px' }}>
                                <Stack direction="row" alignItems="center" onClick={() => handleLanguage('vietnam')}>
                                    <Box
                                        component="img"
                                        alt="vn"
                                        src={images.vnLanguage}
                                        ml="8px"
                                        width="18px"
                                        height="18px"
                                    />
                                    <TypographySubNav>Việt Nam</TypographySubNav>
                                </Stack>
                                <Stack direction="row" alignItems="center" onClick={() => handleLanguage('english')}>
                                    <Box
                                        component="img"
                                        alt="uk"
                                        src={images.ukLanguage}
                                        ml="8px"
                                        width="18px"
                                        height="18px"
                                    />
                                    <TypographySubNav>English</TypographySubNav>
                                </Stack>
                                <Stack direction="row" alignItems="center" onClick={() => handleLanguage('france')}>
                                    <Box
                                        component="img"
                                        alt="fr"
                                        src={images.franceLanguage}
                                        ml="8px"
                                        width="18px"
                                        height="18px"
                                    />
                                    <TypographySubNav>France</TypographySubNav>
                                </Stack>
                            </MenuCustom>
                        </NavbarHaveSubNav>
                    </Stack>
                    <Stack direction="row">
                        {/* Search */}
                        <IconButton
                            disableRipple
                            sx={{ color: '#fff', marginLeft: '8px', '&hover': { backgroundColor: 'transparent' } }}
                        >
                            <OutsideClick handleClicked={setShowSearch}>
                                <InputSearch
                                    placeholder="Search..."
                                    inputProps={{ 'aria-label': 'Search...' }}
                                    inputRef={ref}
                                    // sx={stylesSearch}
                                    sx={
                                        showSearch
                                            ? {
                                                  backgroundColor: '#fff',
                                                  input: {
                                                      width: '140px',
                                                  },
                                                  '.MuiSvgIcon-root': {
                                                      color: '#333',
                                                  },
                                              }
                                            : width !== undefined && width > 0
                                            ? {
                                                  backgroundColor: '#fff',
                                                  '.MuiSvgIcon-root': {
                                                      color: '#333',
                                                  },
                                              }
                                            : {
                                                  backgroundColor: 'transparent',
                                              }
                                    }
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <SearchIcon
                                                fontSize="small"
                                                sx={{ cursor: 'pointer' }}
                                                onClick={handleClickSearchIcon}
                                            />
                                        </InputAdornment>
                                    }
                                />
                            </OutsideClick>
                        </IconButton>

                        {/* Mode */}
                        <IconButton
                            disableRipple
                            sx={{
                                color: '#fff',
                                marginLeft: '8px',
                                display: { xs: 'none', md: 'flex' },
                                '&:hover': { backgroundColor: 'transparent' },
                            }}
                            onClick={handleToggleMode}
                        >
                            {mode === 'light' ? <LightModeIcon fontSize="small" /> : <DarkModeIcon fontSize="small" />}
                        </IconButton>

                        {/* Menu */}
                        <IconButton
                            disableRipple
                            sx={{
                                color: '#fff',
                                display: { xs: 'flex', md: 'none' },
                                '&:hover': { backgroundColor: 'transparent' },
                            }}
                            onClick={() => setShowDrawer(true)}
                        >
                            <MenuIcon />
                        </IconButton>
                    </Stack>

                    {/* Mobile and Tablet Sidebar */}
                    <Drawer anchor="right" open={showDrawer} onClose={handleResetDrawer}>
                        <Box width={{ xs: '220px', sm: '300px' }}>
                            <Stack direction="row" justifyContent="space-between" alignItems="center" height="64px">
                                <IconButton sx={{ marginLeft: '4px' }} onClick={handleResetDrawer}>
                                    <ArrowBackIosNewIcon />
                                </IconButton>
                                <FormControlLabel
                                    control={
                                        <Switch disableRipple checked={mode === 'dark'} onChange={handleToggleMode} />
                                    }
                                    label="Dark Theme"
                                />
                            </Stack>
                            <Stack direction="column">
                                <AccordionCustom
                                    expanded={expanded === 'solution'}
                                    onChange={(event, isExpanded) => handleExplanded(isExpanded, 'solution')}
                                >
                                    <AccordionSummary
                                        id="solution"
                                        aria-controls="solution"
                                        expandIcon={<ExpandMoreIcon />}
                                    >
                                        <HomeIcon />
                                        Solution
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <Box
                                            onClick={() => {
                                                navigate('/');
                                                handleResetDrawer();
                                            }}
                                        >
                                            <Typography>Video Processing</Typography>
                                        </Box>
                                        <Box
                                            onClick={() => {
                                                navigate('/');
                                                handleResetDrawer();
                                            }}
                                        >
                                            <Typography>Security</Typography>
                                        </Box>
                                        <Box
                                            onClick={() => {
                                                navigate('/');
                                                handleResetDrawer();
                                            }}
                                        >
                                            <Typography>OOT</Typography>
                                        </Box>
                                    </AccordionDetails>
                                </AccordionCustom>
                                <SideBarLink
                                    direction="row"
                                    onClick={() => {
                                        navigate('/news');
                                        handleResetDrawer();
                                    }}
                                >
                                    <FeedIcon />
                                    <Typography>News</Typography>
                                </SideBarLink>
                                <SideBarLink
                                    direction="row"
                                    onClick={() => {
                                        navigate('/partner');
                                        handleResetDrawer();
                                    }}
                                >
                                    <GroupWorkIcon />
                                    <Typography>Partner</Typography>
                                </SideBarLink>
                                <SideBarLink
                                    direction="row"
                                    onClick={() => {
                                        navigate('/contact');
                                        handleResetDrawer();
                                    }}
                                >
                                    <PermContactCalendarIcon />
                                    <Typography>Contact</Typography>
                                </SideBarLink>
                                <AccordionCustom
                                    expanded={expanded === 'language'}
                                    onChange={(event, isExpanded) => handleExplanded(isExpanded, 'language')}
                                >
                                    <AccordionSummary
                                        id="language"
                                        aria-controls="language"
                                        expandIcon={<ExpandMoreIcon />}
                                    >
                                        <LanguageIcon />
                                        Language
                                        <Box
                                            component="img"
                                            src={languageLogo}
                                            alt="vn"
                                            ml="4px"
                                            width="18px"
                                            height="18px"
                                        />
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        <Stack
                                            direction="row"
                                            alignItems="center"
                                            onClick={() => handleLanguage('vietnam')}
                                        >
                                            <Box component="img" alt="vn" src={images.vnLanguage} />
                                            <Typography>Việt Nam</Typography>
                                        </Stack>
                                        <Stack
                                            direction="row"
                                            alignItems="center"
                                            onClick={() => handleLanguage('english')}
                                        >
                                            <Box component="img" alt="uk" src={images.ukLanguage} />
                                            <Typography>English</Typography>
                                        </Stack>
                                        <Stack
                                            direction="row"
                                            alignItems="center"
                                            onClick={() => handleLanguage('france')}
                                        >
                                            <Box component="img" alt="fr" src={images.franceLanguage} />
                                            <Typography>France</Typography>
                                        </Stack>
                                    </AccordionDetails>
                                </AccordionCustom>
                            </Stack>
                        </Box>
                    </Drawer>
                </Toolbar>
            </AppBar>
        </ElevationScroll>
    );
}

export default Header;
