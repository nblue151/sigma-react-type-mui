module.exports = {
    plugins: ['prettier'],
    extends: ['airbnb', 'airbnb-typescript', 'react-app', 'react-app/jest', 'prettier'],
    rules: {
        'prettier/prettier': ['warn', { endOfLine: 'auto' }],
    },
    parserOptions: {
        project: './tsconfig.json',
    },
};
